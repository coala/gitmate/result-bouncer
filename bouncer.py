"""
filters json results from stdin and prints unique new ones.
"""
from difflib import SequenceMatcher
import json
import sys

from utils import remove_ranges
from merger import three_way_merge


def process_input():
    """
    reads input from stdin, truncates paths, groups by section, ...
    """
    # read input
    (new_results_by_section,
     new_files_by_section,
     old_results_by_section,
     old_files_by_section) = read_input()

    # join sections for everything besides new results.
    # the files cannot change in between sections (we don't autoapply) and we
    # want to compare new results against all old results anyway.
    new_files = join_section_dict(new_files_by_section)
    old_files = join_section_dict(old_files_by_section)
    old_results = join_section_list(old_results_by_section)

    # adjust all file paths
    adjust_value_paths(new_results_by_section,
                       lambda path: path.split('/', maxsplit=3)[3])
    adjust_value_paths(
        old_results, lambda path: path.split('/', maxsplit=3)[3])

    new_files = adjusted_key_paths(
        new_files, lambda path: path.split('/', maxsplit=3)[3])
    old_files = adjusted_key_paths(
        old_files, lambda path: path.split('/', maxsplit=3)[3])

    # determine correct representation for all filenames
    file_map = create_file_map(new_files, old_files)

    # adjust all file paths
    adjust_value_paths(new_results_by_section, file_map)
    adjust_value_paths(old_results, file_map)

    new_files = adjusted_key_paths(new_files, file_map)
    old_files = adjusted_key_paths(old_files, file_map)

    return (new_results_by_section,
            new_files,
            old_results,
            old_files)


def read_input():
    """
    read json input from stdin
    """
    stdinput = json.load(sys.stdin)
    return (stdinput['new_results'], stdinput['new_files'],
            stdinput['old_results'], stdinput['old_files'])


def join_section_dict(section_dict):
    joined_dict = {}
    for _, items in section_dict.items():
        joined_dict.update(items)
    return joined_dict


def join_section_list(section_list):
    joined_list = []
    for _, items in section_list.items():
        joined_list.extend(items)
    return joined_list


def create_file_map(new_files, old_files):
    """
    determine correct representation for all filenames
    adjust for renamed and new files
    deleted files don't matter because a new result can't affect code there.
    Also truncates 2 dirs from the beginning of every file name for reasons
    """
    new_file_names = set(new_files.keys())
    old_file_names = set(old_files.keys())

    unchanged_file_names = new_file_names & old_file_names
    deleted_file_names = old_file_names - new_file_names
    created_file_names = new_file_names - old_file_names

    file_map = {}

    # refer to unchanged and new files by their new/real names
    for file_name in unchanged_file_names | created_file_names:
        file_map[file_name] = file_name

    # refer to moved files by their new names
    # first round: Check for absolute equality
    for created in list(created_file_names):
        for candidate in deleted_file_names:
            if new_files[created] == old_files[candidate]:
                file_map[candidate] = created
                created_file_names.remove(created)
                deleted_file_names.remove(candidate)
                break

    # second round: Take the one with the best ratio as long as it's >0.5
    for created in created_file_names:
        best_match = ''
        best_ratio = 0.5
        for candidate in deleted_file_names:
            smatcher = SequenceMatcher(
                None,
                "".join(new_files[created]),
                "".join(old_files[candidate]))
            if smatcher.real_quick_ratio() >= best_ratio:
                ratio = smatcher.ratio()
                if ratio > best_ratio:
                    best_ratio = ratio
                    best_match = candidate

        if best_match:
            file_map[best_match] = created
            deleted_file_names.remove(best_match)

    # refer to deleted files by their old names
    for deleted in deleted_file_names:
        file_map[deleted] = deleted

    return lambda x: file_map[x]


def adjust_value_paths(elem, file_map):
    """
    Deletes the first two dirs from all file paths:
    - in values of dicts where the key is "file"
    - in keys of dicts that begin with '/'
    """
    if isinstance(elem, dict):
        for key, value in elem.items():
            if isinstance(value, str) and key == 'file':
                elem[key] = file_map(value)
            else:
                adjust_value_paths(value, file_map)
    elif isinstance(elem, list):
        for _elem in elem:
            adjust_value_paths(_elem, file_map)


def adjusted_key_paths(elem, file_map):
    adjusted = {}
    for key, value in elem.items():
        adjusted[file_map(key)] = value
    return adjusted


def direct_hashes(results, files):
    """
    calculates a set of hashes from the results and files
    """
    hashes = set()

    for result in results:
        affected_ranges_by_file = {}

        for srange in result["affected_code"]:
            range_tuple = (srange["start"]["line"], srange["start"]["column"],
                           srange["end"]["line"], srange["end"]["column"])
            if srange["file"] in affected_ranges_by_file:
                affected_ranges_by_file[srange["file"]].append(range_tuple)
            else:
                affected_ranges_by_file[srange["file"]] = [range_tuple]

        files_without_ranges = {}
        for file_name in affected_ranges_by_file:
            files_without_ranges[file_name] = remove_ranges(
                files[file_name],
                affected_ranges_by_file[file_name])

        hashes.add(_hash(result, files_without_ranges))

    return hashes


def _hash(result, relevant_file_dict):
    basics = [result["aspect"],
              result["origin"],
              result["message"],
              result["debug_msg"],
              result["severity"],
              result["confidence"],
              result["additional_info"]]
    files = list(sorted(relevant_file_dict.items()))
    # cannot have lists or dicts, only tuples:
    files = []
    for file_name, file_content in sorted(relevant_file_dict.items()):
        files.extend((file_name, tuple(file_content)))

    return hash(tuple(basics + files))


def diff_hashes(results, files, old_files):
    """
    returns a list of tuples ((hash, result) for every result)
    """
    hash_tuples = []

    for result in results:
        affected_ranges_by_file = {}

        for srange in result["affected_code"]:
            range_tuple = (srange["start"]["line"], srange["start"]["column"],
                           srange["end"]["line"], srange["end"]["column"])
            if srange["file"] in affected_ranges_by_file:
                affected_ranges_by_file[srange["file"]].append(range_tuple)
            else:
                affected_ranges_by_file[srange["file"]] = [range_tuple]

        try:
            merged_files_without_ranges = {}
            for file_name in affected_ranges_by_file:
                new_file_without_ranges = remove_ranges(files[file_name],
                                                        affected_ranges_by_file[file_name])
                old_file_without_new_ranges = three_way_merge(files[file_name],
                                                              new_file_without_ranges,
                                                              old_files.get(file_name, []))
                merged_files_without_ranges[file_name] = old_file_without_new_ranges
            hash_tuples.append(
                (_hash(result, merged_files_without_ranges), result))

        except ValueError:
            # hash == None cannot happen for old results (no merging)
            # this hash is therefore unique and the result is being returned by the bouncer
            # which is exactly what we want here, since the file changes happen inside
            # the affected code area
            hash_tuples.append((None, result))
    return hash_tuples


def filter_results(hash_result_tuples, old_hashes):
    """
    returns a list of such results that have a unique hash or where the hash
    creation failed because the file was changed inside of a result's
    affected_code
    """

    return [result for _hash, result in filter(
        lambda tpl: tpl[0] not in old_hashes,
        hash_result_tuples)]


if __name__ == '__main__':
    (new_results_by_section,
     new_files,
     old_results,
     old_files) = process_input()

    old_hashes = direct_hashes(old_results,
                               old_files)
    unique_new_results = {}

    for section in new_results_by_section:
        hash_result_tuples = diff_hashes(new_results_by_section[section],
                                         new_files,
                                         old_files)
        new_results = filter_results(hash_result_tuples, old_hashes)
        unique_new_results[section] = new_results

    print(json.dumps(unique_new_results))
