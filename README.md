ResultBouncer uses some coala magic to differentiate such results that are new from such that have been there before. Everybody is allowed to party just once.

## try it yourself

In the result-bouncer folder, run: `python3 bouncer.py < test-input.json`

## Input / Output

The bouncer expects to be passed one json string via stdin.
It should contain the dicts of filename:filecontent for the
project at two different points in time, as well as the list
of results detected by coala for the respective set of files.

The following content is crucial. Further fields are allowed,
but will be ignored (like a result's "id" or "diff").

```
{
    "old_files" : {
        "default": {
            "file_name_1": [ "line_1", "line_2", "line_3", ... ],
            "file_name_2": [ "line_1", "line_2", "line_3", ... ],
            ...
        }
    },
    "new_files": {
        "default": {
            "file_name_1": [ "line_1", "line_2", "line_3", ... ],
            "file_name_2": [ "line_1", "line_2", "line_3", ... ],
            ...
        }
    },
    "old_results": {
        "default":[
            {
                "additional_info": "",
                "affected_code": [
                    {
                        "end": {
                            "column": null,
                            "file": "/home/fabian/devel/coala-incremental/run_coala.py",
                            "line": 43
                        },
                        "file": "/home/fabian/devel/coala-incremental/run_coala.py",
                        "start": {
                            "column": null,
                            "file": "/home/fabian/devel/coala-incremental/run_coala.py",
                            "line": 43
                        }
                    }
                ],
                "aspect": "NoneType",
                "confidence": 100,
                "debug_msg": "",
                "diffs": {
                    "/home/fabian/devel/coala-incremental/run_coala.py": "--- \n+++ \n@@ -40,7 +40,8 @@\n     oldargv = copy(sys.argv)\n     sys.argv.clear()\n     sys.argv.extend(['coala', '-c='+project_dir])\n-    results, _, file_dicts = run_coala(log_printer=log_printer, autoapply=False)\n+    results, _, file_dicts = run_coala(\n+        log_printer=log_printer, autoapply=False)\n     sys.argv.clear()\n     sys.argv.extend(oldargv)\n \n"
                        },
                "id": 314184076505727882470034755948244365195,
                "message": "The code does not comply to PEP8.",
                "origin": "PEP8Bear",
                "severity": 1
            },
            {
                ...
            },
            ...
        ],
        "python":[
            {
                ...
            },
        ]
    },
    "new_results": {
        "default":[
            {
                ...
            },
            {
            ...
            },
            ...
        ],
        "python":[
            {
                ....
            }
        ]
    }
}

```
