FROM python:3
MAINTAINER Naveen Kumar Sangi nkprince007@gmail.com

RUN pip install coala

RUN mkdir -p /usr/src/app
COPY . /usr/src/app

WORKDIR /usr/src/app
